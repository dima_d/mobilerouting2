package com.kris.details2.contract.di

import com.kris.details2.mainDI.Details2MainDIComponent

interface Details2MainDIComponentProvider {

    fun diskMainDIComponent(): Details2MainDIComponent
}