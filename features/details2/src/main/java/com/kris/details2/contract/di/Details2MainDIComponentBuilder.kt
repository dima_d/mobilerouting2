package com.kris.details2.contract.di

import com.kris.common.di.FeatureModuleDIComponentBuilder
import com.kris.common.di.main.CommonMainDIComponent
import com.kris.details2.contract.dependencies.Details2Dependencies
import com.kris.details2.mainDI.DaggerDetails2MainDIComponent
import com.kris.details2.mainDI.Details2MainDIComponent

class Details2MainDIComponentBuilder(
    commonMainDIComponent: CommonMainDIComponent,
    dependencies: Details2Dependencies
) : FeatureModuleDIComponentBuilder<Details2MainDIComponent, Details2Dependencies>(commonMainDIComponent, dependencies) {

    override fun buildComponentImmediately(): Details2MainDIComponent =
        DaggerDetails2MainDIComponent.builder()
            .commonMainDIComponent(commonMainDIComponent)
            .diskDependencies(dependencies)
            .build()
}