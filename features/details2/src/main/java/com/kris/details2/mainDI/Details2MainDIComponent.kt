package com.kris.details2.mainDI

import android.content.Context
import com.kris.common.di.main.CommonExposedDI
import com.kris.common.di.main.CommonMainDIComponent
import com.kris.details2.contract.dependencies.Details2Dependencies
import com.kris.details2.contract.di.Details2MainDIComponentProvider
import dagger.BindsInstance
import dagger.Component

@Details2MainDIScope
@Component(dependencies = [CommonMainDIComponent::class], modules = [Details2MainDIModule::class])
interface Details2MainDIComponent : CommonExposedDI {

    fun diskDependencies(): Details2Dependencies

    @Component.Builder
    interface Builder {

        fun commonMainDIComponent(commonMainDIComponent: CommonMainDIComponent): Builder

        @BindsInstance
        fun diskDependencies(diskDependencies: Details2Dependencies): Builder

        fun build(): Details2MainDIComponent
    }

    companion object {
        internal fun fromContext(context: Context): Details2MainDIComponent =
            (context.applicationContext as Details2MainDIComponentProvider).diskMainDIComponent()
    }
}