package com.kris.details2.mainDI

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
internal annotation class Details2MainDIScope