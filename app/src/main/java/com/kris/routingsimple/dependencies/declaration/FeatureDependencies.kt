package com.kris.routingsimple.dependencies.declaration

import com.kris.api.RoutingFeature

interface FeatureDependencies :
    ModularDependencies,
    AppDependencies,
    RoutingFeature