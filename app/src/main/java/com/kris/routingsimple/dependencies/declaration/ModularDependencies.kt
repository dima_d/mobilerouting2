package com.kris.routingsimple.dependencies.declaration

import com.kris.details1.contract.dependencies.Details1Dependencies
import com.kris.details2.contract.dependencies.Details2Dependencies

interface ModularDependencies :
    Details2Dependencies,
    Details1Dependencies
