package com.kris.routingsimple.dependencies

import com.kris.api.Details1Feature
import com.kris.api.Details2Feature
import com.kris.api.RoutingFeature
import com.kris.details1.contract.feature.Details1FeatureDefaultImpl
import com.kris.details2.contract.feature.Details2FeatureDefaultImpl
import com.kris.routingsimple.dependencies.declaration.FeatureDependencies

class FeatureDependenciesManager(
    private val details2Feature: Details2Feature = Details2FeatureDefaultImpl(),
    private val details1Feature: Details1Feature = Details1FeatureDefaultImpl()
    ) :
    Details2Feature by details2Feature,
    Details1Feature by details1Feature,
    FeatureDependencies