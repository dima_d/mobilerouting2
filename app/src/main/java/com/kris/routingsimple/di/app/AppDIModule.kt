package com.kris.routingsimple.di.app

import com.kris.api.RouterInteractor
import com.kris.routingsimple.app_router.AppRouter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

@Module
class AppDIModule{
    private var cicerone: Cicerone<Router> = Cicerone.create()

    @Provides
    @AppDIScope
    internal fun provideRouter(): RouterInteractor {
        return AppRouter(cicerone)
    }

    @Provides
    @AppDIScope
    internal fun provideNavigatorHolder(): NavigatorHolder {
        return cicerone.navigatorHolder
    }

}