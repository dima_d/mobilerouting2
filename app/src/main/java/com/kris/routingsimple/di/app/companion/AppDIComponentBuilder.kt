package com.kris.routingsimple.di.app.companion

import android.app.Application
import com.kris.common.di.DIComponentBuilder
import com.kris.routingsimple.di.app.AppDIComponent
import com.kris.routingsimple.di.app.DaggerAppDIComponent
import com.kris.routingsimple.di.modular.ModularDIComponentsDIModule

class AppDIComponentBuilder(private val application: Application) : DIComponentBuilder<AppDIComponent>() {

    override fun buildComponentImmediately(): AppDIComponent =
        DaggerAppDIComponent.builder()
            .modularDIComponentsDIModule(ModularDIComponentsDIModule(application))
            .build()
}