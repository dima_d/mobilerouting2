package com.kris.routingsimple.di.app.companion

import com.kris.routingsimple.di.app.AppDIComponent

interface AppDIComponentProvider {

    fun appDIComponent(): AppDIComponent
}