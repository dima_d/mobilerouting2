package com.kris.routingsimple.di.app

import android.content.Context
import com.kris.routingsimple.dependencies.declaration.AppDependencies
import com.kris.routingsimple.di.app.companion.AppDIComponentProvider
import com.kris.routingsimple.di.dependencies.FeatureDependenciesDIModule
import com.kris.routingsimple.di.modular.ModularDIComponentsDIModule
import com.kris.routingsimple.di.modular.ModularDIComponentsProvider
import dagger.Component

@AppDIScope
@Component(modules = [FeatureDependenciesDIModule::class, ModularDIComponentsDIModule::class, AppDIModule::class])
interface AppDIComponent : ModularDIComponentsProvider {

    fun appDependencies(): AppDependencies

    companion object {
        fun fromContext(context: Context): AppDIComponent =
            (context.applicationContext as AppDIComponentProvider).appDIComponent()
    }

    @Component.Builder
    interface Builder {
        fun modularDIComponentsDIModule(modularDIComponentsDIModule: ModularDIComponentsDIModule): Builder

        fun build(): AppDIComponent
    }

}