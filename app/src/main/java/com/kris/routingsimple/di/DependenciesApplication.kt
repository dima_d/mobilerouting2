package com.kris.routingsimple.di

import android.app.Application
import com.kris.api.RouterInteractor
import com.kris.common.di.main.CommonMainDIComponent
import com.kris.routingsimple.di.app.AppDIComponent
import com.kris.routingsimple.di.app.companion.AppDIComponentBuilder
import com.kris.routingsimple.di.app.companion.AppDIComponentProvider
import com.kris.routingsimple.di.modular.ModularDIComponentsProvider
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router


class DependenciesApplication :
    Application(),
    AppDIComponentProvider,
    ModularDIComponentsProvider {

    private val appDIComponent: AppDIComponent = AppDIComponentBuilder(this).buildComponent()

    //region AppDIComponentProvider
    override fun appDIComponent(): AppDIComponent = appDIComponent
    //endregion

    //region ModularDIComponentsProvider
    override fun commonMainDIComponent(): CommonMainDIComponent =
        appDIComponent.commonMainDIComponent()

    override fun provideRouter(): RouterInteractor = appDIComponent.provideRouter()
    override fun provideNavigatorHolder(): NavigatorHolder = appDIComponent.provideNavigatorHolder()

}