package com.kris.routingsimple.di.modular

import android.app.Application
import com.kris.common.contract.di.CommonMainDIComponentBuilder
import com.kris.common.di.main.CommonMainDIComponent
import com.kris.details1.contract.di.Details1MainDIComponentBuilder
import com.kris.details1.mainDI.Details1MainDIComponent
import com.kris.details2.contract.di.Details2MainDIComponentBuilder
import com.kris.details2.mainDI.Details2MainDIComponent
import com.kris.routingsimple.dependencies.declaration.ModularDependencies
import com.kris.routingsimple.di.app.AppDIScope
import dagger.Module
import dagger.Provides

@Module
class ModularDIComponentsDIModule(private val application: Application) {

    @Provides
    @AppDIScope
    fun commonMainDIComponent(): CommonMainDIComponent = CommonMainDIComponentBuilder(application).buildComponent()

    @Provides
    @AppDIScope
    fun details2MainDIComponent(
        commonMainDIComponent: CommonMainDIComponent,
        modularDependencies: ModularDependencies
    ): Details2MainDIComponent =
        Details2MainDIComponentBuilder(commonMainDIComponent, modularDependencies).buildComponent()

    @Provides
    @AppDIScope
    fun details1MainDIComponent(
        commonMainDIComponent: CommonMainDIComponent,
        modularDependencies: ModularDependencies
    ): Details1MainDIComponent =
        Details1MainDIComponentBuilder(commonMainDIComponent, modularDependencies).buildComponent()

}