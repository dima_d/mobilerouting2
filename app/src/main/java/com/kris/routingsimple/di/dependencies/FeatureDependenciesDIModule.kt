package com.kris.routingsimple.di.dependencies

import com.kris.routingsimple.dependencies.declaration.AppDependencies
import com.kris.routingsimple.dependencies.declaration.FeatureDependencies
import com.kris.routingsimple.dependencies.FeatureDependenciesManager
import com.kris.routingsimple.dependencies.declaration.ModularDependencies
import com.kris.routingsimple.di.app.AppDIScope
import dagger.Module
import dagger.Provides

@Module
class FeatureDependenciesDIModule {

    @Provides
    @AppDIScope
    fun featureDependencies(): FeatureDependencies = FeatureDependenciesManager()

    @Provides
    @AppDIScope
    fun modularDependencies(featureDependencies: FeatureDependencies): ModularDependencies = featureDependencies

    @Provides
    @AppDIScope
    fun appDependencies(featureDependencies: FeatureDependencies): AppDependencies = featureDependencies
}