package com.kris.routingsimple.di.modular

import com.kris.common.contract.di.CommonMainDIComponentProvider

interface ModularDIComponentsProvider :
    CommonMainDIComponentProvider
