package com.kris.routingsimple.di.app

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class AppDIScope