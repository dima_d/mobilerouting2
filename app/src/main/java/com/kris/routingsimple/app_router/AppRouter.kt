package com.kris.routingsimple.app_router

import android.util.Log
import com.kris.api.RouterInteractor
import com.kris.details1.routing.Details1RoutingModule
import com.kris.router.RouterInteractorImpl
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router

class AppRouter(private val cicerone: Cicerone<Router>) :
    RouterInteractor by RouterInteractorImpl(cicerone.router) {
    private val TAG = AppRouter::class.java.simpleName

    init {
        enableLogging(TAG)
        putModule(Details1RoutingModule(this))

    }

}