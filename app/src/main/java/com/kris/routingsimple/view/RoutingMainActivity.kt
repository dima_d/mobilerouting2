package com.kris.routingsimple.view

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.util.Log
import com.kris.api.Details1StartList
import com.kris.common.BaseActivity
import com.kris.common.contract.di.CommonMainDIComponentProvider
import com.kris.routingsimple.R
import com.kris.details1.routing.StartScreen
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import ru.terrakok.cicerone.commands.Replace

class RoutingMainActivity : BaseActivity() {

    val router by lazy { (applicationContext as CommonMainDIComponentProvider).provideRouter() }
    override val navigator by lazy { SupportAppNavigator(this, R.id.main_container) }


    private val viewModel: RoutingMainViewModel by lazy {
        val routingMainViewModelFactory = RoutingMainViewModelFactory()
        ViewModelProviders.of(this, routingMainViewModelFactory).get(RoutingMainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("ddd", "onCreate ")
        setContentView(R.layout.main_activity)
        router.fire(Details1StartList())

//        navigator.applyCommands(arrayOf<Command>(Replace(StartScreen)))
    }

}