package com.kris.routingsimple.view

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.SharedPreferences

internal class RoutingMainViewModelFactory(

) : ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = RoutingMainViewModel() as T
}

class RoutingMainViewModel:ViewModel() {



}