package com.kris.common

import android.support.v4.app.FragmentActivity
import com.kris.common.contract.di.CommonMainDIComponentProvider
import ru.terrakok.cicerone.Navigator

abstract class BaseActivity : FragmentActivity() {

    abstract val navigator: Navigator
    private val navigatorHolder by lazy { (applicationContext as CommonMainDIComponentProvider).provideNavigatorHolder() }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

}