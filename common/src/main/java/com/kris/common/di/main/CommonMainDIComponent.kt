package com.kris.common.di.main

import android.content.Context
import com.kris.api.RouterInteractor
import com.kris.common.contract.di.CommonMainDIComponentProvider
import com.kris.common.model.CommonModel
import dagger.Component

@CommonMainDIScope
@Component(modules = [CommonMainDIModule::class])
interface CommonMainDIComponent : CommonExposedDI {

    fun provideRouter(): RouterInteractor
    fun provideModel(): CommonModel

    companion object {
        fun fromContext(context: Context): CommonMainDIComponent =
            (context.applicationContext as CommonMainDIComponentProvider).commonMainDIComponent()
    }
}