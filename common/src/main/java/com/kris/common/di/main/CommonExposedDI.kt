package com.kris.common.di.main

import android.content.Context

interface CommonExposedDI {
    
    fun applicationContext(): Context
}