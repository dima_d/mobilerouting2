package com.kris.common.di

import com.kris.common.di.main.CommonMainDIComponent

abstract class FeatureModuleDIComponentBuilder<C, D>(
    protected val commonMainDIComponent: CommonMainDIComponent,
    protected val dependencies: D
) : DIComponentBuilder<C>()