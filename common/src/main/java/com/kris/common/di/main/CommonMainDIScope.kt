package com.kris.common.di.main

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
internal annotation class CommonMainDIScope