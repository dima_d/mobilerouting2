package com.kris.common.di.main

import android.app.Application
import android.content.Context
import com.kris.api.RouterInteractor
import com.kris.common.contract.di.CommonMainDIComponentProvider
import com.kris.common.model.CommonModel
import dagger.Module
import dagger.Provides

@Module
internal class CommonMainDIModule(private val application: Application) {

    private val provider = application as CommonMainDIComponentProvider

    @Provides
    @CommonMainDIScope
    internal fun provideModel(): CommonModel  = CommonModel()

        @Provides
    @CommonMainDIScope
    internal fun provideRouter(): RouterInteractor {
        return provider.provideRouter()
    }

    @Provides
    fun applicationContext(): Context = application.applicationContext
}