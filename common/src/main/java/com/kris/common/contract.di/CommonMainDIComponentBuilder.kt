package com.kris.common.contract.di

import android.app.Application
import com.kris.common.di.main.CommonMainDIComponent
import com.kris.common.di.main.CommonMainDIModule
import com.kris.common.di.DIComponentBuilder
import com.kris.common.di.main.DaggerCommonMainDIComponent

class CommonMainDIComponentBuilder(private val application: Application) : DIComponentBuilder<CommonMainDIComponent>() {

    override fun buildComponentImmediately(): CommonMainDIComponent =
        DaggerCommonMainDIComponent.builder()
            .commonMainDIModule(CommonMainDIModule(application))
            .build()
}