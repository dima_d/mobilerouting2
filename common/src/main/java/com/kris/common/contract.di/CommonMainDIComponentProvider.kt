package com.kris.common.contract.di

import com.kris.api.RouterInteractor
import com.kris.common.di.main.CommonMainDIComponent
import ru.terrakok.cicerone.NavigatorHolder

interface CommonMainDIComponentProvider {

    fun commonMainDIComponent(): CommonMainDIComponent
    fun provideRouter(): RouterInteractor
    fun provideNavigatorHolder(): NavigatorHolder
}