package com.kris.details1.routing

import com.kris.api.Details1StartItem
import com.kris.api.Details1StartList
import com.kris.api.RouterInteractor
import com.kris.api.RouterModule

class Details1RoutingModule(routerInteractor: RouterInteractor) : RouterModule(routerInteractor.router) {
    init {
        event(Details1StartList::class) {
            router.navigateTo(StartScreen)
            router.newChain()
        }

        event(Details1StartItem::class){


        }
    }
}