package com.kris.details1.contract.di

import com.kris.details1.mainDI.Details1MainDIComponent

interface Details1MainDIComponentProvider {

    fun details1MainDIComponent(): Details1MainDIComponent
}