package com.kris.details1.mainDI

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
internal annotation class Details1MainDIScope