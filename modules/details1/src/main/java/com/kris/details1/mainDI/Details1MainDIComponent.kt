package com.kris.details1.mainDI

import android.content.Context
import com.kris.common.di.main.CommonExposedDI
import com.kris.common.di.main.CommonMainDIComponent
import com.kris.details1.contract.dependencies.Details1Dependencies
import com.kris.details1.contract.di.Details1MainDIComponentProvider
import dagger.BindsInstance
import dagger.Component

@Details1MainDIScope
@Component(dependencies = [CommonMainDIComponent::class], modules = [Details1MainDIModule::class])
interface Details1MainDIComponent : CommonExposedDI {

    fun details1Dependencies(): Details1Dependencies

    @Component.Builder
    interface Builder {

        fun commonMainDIComponent(commonMainDIComponent: CommonMainDIComponent): Builder

        @BindsInstance
        fun details1Dependencies(details1Dependencies: Details1Dependencies): Builder

        fun build(): Details1MainDIComponent
    }

    companion object {
        internal fun fromContext(context: Context): Details1MainDIComponent =
            (context.applicationContext as Details1MainDIComponentProvider).details1MainDIComponent()
    }
}