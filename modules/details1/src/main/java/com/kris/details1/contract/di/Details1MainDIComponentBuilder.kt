package com.kris.details1.contract.di

import android.util.Log
import com.kris.common.di.FeatureModuleDIComponentBuilder
import com.kris.common.di.main.CommonMainDIComponent
import com.kris.details1.contract.dependencies.Details1Dependencies
import com.kris.details1.mainDI.DaggerDetails1MainDIComponent
import com.kris.details1.mainDI.Details1MainDIComponent
import com.kris.details1.routing.Details1RoutingModule

class Details1MainDIComponentBuilder(
    commonMainDIComponent: CommonMainDIComponent,
    dependencies: Details1Dependencies
) : FeatureModuleDIComponentBuilder<Details1MainDIComponent, Details1Dependencies>(commonMainDIComponent, dependencies) {

    override fun buildComponentImmediately(): Details1MainDIComponent {
       return DaggerDetails1MainDIComponent.builder()
            .commonMainDIComponent(commonMainDIComponent)
            .details1Dependencies(dependencies)
            .build()
    }
}