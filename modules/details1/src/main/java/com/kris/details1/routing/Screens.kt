package com.kris.details1.routing

import android.content.Context
import android.content.Intent
import android.support.v4.app.Fragment
import com.kris.details1.ItemDetailActivity
import com.kris.details1.ItemDetailFragment
import com.kris.details1.ItemListFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object StartScreen: SupportAppScreen(){
    override fun getFragment(): Fragment = ItemListFragment()
}

object ItemDetailFragmentScreen: SupportAppScreen(){

    override fun getFragment(): Fragment = ItemDetailFragment()
}

class ItemDetailActivityScreen: SupportAppScreen(){
    override fun getActivityIntent(context: Context?): Intent
     = Intent(context, ItemDetailActivity::class.java).apply {  }
}