package com.kris.api

import com.kris.declaration.RouterEvent
import ru.terrakok.cicerone.Router
import java.util.concurrent.ConcurrentHashMap
import kotlin.reflect.KClass

interface RouterInteractor {
    val isTablet:Boolean get() = false
    val router:Router
    fun fire(event: RouterEvent)
    fun enableLogging(tag: String)
    fun stop()
    fun putModule(routerModule: RouterModule): RouterModule?
    fun getModule(module: KClass<out RouterModule>): RouterModule?
}


abstract class RouterModule(val router: Router) {
    private val map = ConcurrentHashMap<KClass<out RouterEvent>, (t: Any) -> Unit>()
    fun <T : RouterEvent> fire(event: T) = (map[event::class] as? (t: T) -> Unit)?.invoke(event)
    fun <T : RouterEvent> contains(event: T) = map.containsKey(event::class)

    @Suppress("UNCHECKED_CAST")
    fun <T : RouterEvent> event(clazz: KClass<T>, consumer: (t: T) -> Unit) =
        map.put(clazz, consumer as (t: Any) -> Unit)
}

