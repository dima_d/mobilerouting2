package com.kris.api.utils

import android.util.Log
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.ObservableTransformer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

operator fun CompositeDisposable.plusAssign(d: Disposable) {
    add(d)
}


/**
 * Функция логирует и позворяет увидеть вживую жизненный цикл подписки
 */
@Deprecated("Использовать только для отладки, удалять перед мерджем!", ReplaceWith(" // отладочный лог удален"))
fun <T : Any> Observable<T>.log(tag: String): Observable<T> = compose(LoggerTransformer(tag))

class LoggerTransformer<T>(val tag: String) : ObservableTransformer<T, T> {
    override fun apply(upstream: Observable<T>): ObservableSource<T> = upstream
        .doOnSubscribe { Log.d(tag, "$tag doOnSubscribe ") }
        .doOnComplete { Log.d(tag, "$tag doOnComplete ") }
        .doOnNext { Log.d(tag, "$tag doOnNext $it") }
        .doOnEach { Log.d(tag, "$tag doOnEach $it") }
        .doOnTerminate { Log.d(tag, "$tag doOnTerminate ") }
        .doOnDispose { Log.d(tag, "$tag doOnDispose ") }
        .doOnError { Log.d(tag, "$tag doOnError $it"); it.printStackTrace() }
}