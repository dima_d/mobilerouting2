package com.kris.api.utils

import android.content.Context
import android.graphics.Color
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.util.*

class UtilsJava
object Utils {

    fun bytesToHexString(bytes: ByteArray, useSeparator: Boolean): String {
        val hexArray = charArrayOf('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F')
        val hexChars = CharArray(bytes.size * 3)
        var c: Int
        for (i in bytes.indices) {
            c = bytes[i].toInt() and 0xFF
            hexChars[i * 3] = hexArray[c.ushr(4)]
            hexChars[i * 3 + 1] = hexArray[c and 0x0F]
            if (i % 8 == 7 && useSeparator) {
                hexChars[i * 3 + 2] = '|'
            } else {
                hexChars[i * 3 + 2] = ' '
            }
        }
        return String(hexChars)
    }
}

fun isVerySmallScreen(context: Context) = context.resources?.displayMetrics?.density ?: 2f < 1f
fun isVeryNarrowScreen(context: Context) = ((context.resources?.displayMetrics?.heightPixels?:1000) / (context.resources?.displayMetrics?.ydpi?:1f)) < 300f
fun toHexaString(color: Int) = String.format("#%06X", 0xFFFFFF and color)

    private var sColorNameMap = HashMap<String, Int>().apply {
       this["black"] = Color.BLACK
       this["darkgray"] = Color.DKGRAY
       this["gray"] = Color.GRAY
       this["lightgray"] = Color.LTGRAY
       this["white"] = Color.WHITE
       this["red"] = Color.RED
       this["green"] = Color.GREEN
       this["blue"] = Color.BLUE
       this["yellow"] = Color.YELLOW
       this["cyan"] = Color.CYAN
       this["magenta"] = Color.MAGENTA
       this["aqua"] = 0xFF00FFFF.toInt()
       this["fuchsia"] = 0xFFFF00FF.toInt()
       this["darkgrey"] = Color.DKGRAY
       this["grey"] = Color.GRAY
       this["lightgrey"] = Color.LTGRAY
       this["lime"] = 0xFF00FF00.toInt()
       this["maroon"] = 0xFF800000.toInt()
       this["navy"] = 0xFF000080.toInt()
       this["olive"] = 0xFF808000.toInt()
       this["purple"] = 0xFF800080.toInt()
       this["silver"] = 0xFFC0C0C0.toInt()
       this["teal"] = 0xFF008080.toInt()

    }

    /**
     * Parse the color string, and return the corresponding color-int.
     * If the string cannot be parsed, throws an IllegalArgumentException
     * exception. Supported formats are:
     * #RRGGBB
     * #AARRGGBB
     * or one of the following names:
     * 'red', 'blue', 'green', 'black', 'white', 'gray', 'cyan', 'magenta',
     * 'yellow', 'lightgray', 'darkgray', 'grey', 'lightgrey', 'darkgrey',
     * 'aqua', 'fuchsia', 'lime', 'maroon', 'navy', 'olive', 'purple',
     * 'silver', 'teal'.
     */
    fun parseColor(colorString: String): Int {
        if (colorString[0] == '#') {
            // Use a long to avoid rollovers on #ffXXXXXX
            var color = java.lang.Long.parseLong(colorString.substring(1), 16)
            if (colorString.length == 4) {
                return Color.rgb((color or 0xf00).toInt()*0x10, (color or 0x0f0).toInt()*0x10, (color or 0x00f).toInt()*0x10)
            } else if (colorString.length == 7) {
                // Set the alpha value
                color = color or 0x00000000ff000000
            } else if (colorString.length != 9) {
                throw IllegalArgumentException("Unknown color")
            }
            return color.toInt()
        } else {
            val color = sColorNameMap.get(colorString.toLowerCase(Locale.ROOT))
            if (color != null) {
                return color
            }
        }
        throw IllegalArgumentException("Unknown color")
    }

fun runOnMainThread(func:()->Unit) = Observable.just(1).observeOn(AndroidSchedulers.mainThread()).subscribe { func.invoke() }

var CompositeDisposable.add:Disposable? set(value) { value?.let { add(it) } } get() = null




