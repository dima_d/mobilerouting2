package com.kris.router.cicerone.navigator

import android.support.v4.app.FragmentManager
import java.util.*

class LocalStack(): Deque<String> by LinkedList<String>() {

    fun copyStackToLocal(fragmentManager: FragmentManager) {
        clear()
        for (i in 0 until fragmentManager.backStackEntryCount) {
            fragmentManager.getBackStackEntryAt(i).name?.let { add(it) }
        }
    }


}