package com.kris.router.cicerone

import ru.terrakok.cicerone.Screen
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.terrakok.cicerone.commands.Command

enum class ScreenTarget{
    FIRST, SECOND, BOTTOM, TOP, RIGHT_PANEL, LEFT_PANEL
}

interface TargetCommand:Command{
    val target:ScreenTarget
}


class Replace(val screen: SupportAppScreen, override val target: ScreenTarget) : TargetCommand
class Forward(val screen: SupportAppScreen, override val target: ScreenTarget) : TargetCommand
class BackTo(val screen: SupportAppScreen, override val target: ScreenTarget) : TargetCommand
class Back(override val target: ScreenTarget) : TargetCommand
