package com.kris.router.cicerone.navigator

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.*
import com.kris.router.cicerone.*
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.terrakok.cicerone.commands.Command


/**
 * Navigator implementation for launch fragments and activities.<br></br>
 * Feature [BackTo] works only for fragments.<br></br>
 * Recommendation: most useful for Single-Activity application.
 */
class TargetNavigator(
    activity: FragmentActivity,
    private val fragmentManager: FragmentManager = activity.supportFragmentManager,
    vararg targets: Pair<ScreenTarget, Int>
) :
    Navigator {

    private val targetMap: Map<ScreenTarget, Int> = HashMap<ScreenTarget, Int>().apply {
        putAll(targets.toMap())
    }


    private val activity: SupportActivity
    private val localStackCopy = LocalStack()

    init {
        this.activity = activity
    }

    override fun applyCommands(commands: Array<Command>) {
        fragmentManager.executePendingTransactions()

        //copy stack before apply commands
        localStackCopy.copyStackToLocal(fragmentManager)

        commands.forEach { (it as? TargetCommand)?.let { it1 -> applyCommand(it1) } }
    }

    /**
     * Perform transition described by the navigation command
     *
     * @param command the navigation command to apply
     */
    protected fun applyCommand(command: Command) {
        when (command) {
            is Forward -> activityForward(command)
            is Replace -> activityReplace(command)
            is BackTo -> backTo(command)
            is Back -> fragmentBack()
            else -> throw RuntimeException("${command::class.java} is not available in ${this::class.java}")
        }
    }


    protected fun activityForward(command: Forward) {
        val screen = command.screen
        val activityIntent = screen.getActivityIntent(activity)

        // Start activity
        if (activityIntent != null) {
            val options = createStartActivityOptions(command, activityIntent)
            checkAndStartActivity(screen, activityIntent, options)
        } else {
            fragmentForward(command)
        }
    }

    private fun getContainerId(target: ScreenTarget) =
        targetMap[target] ?: throw RuntimeException("Target is not set in this navigator")

    protected fun fragmentForward(command: Forward) {
        val screen = command.screen
        val fragment = createFragment(screen)

        val fragmentTransaction = fragmentManager.beginTransaction()

        setupFragmentTransaction(
            command,
            fragmentManager.findFragmentById(getContainerId(command.target)),
            fragment,
            fragmentTransaction
        )

        fragmentTransaction
            .replace(getContainerId(command.target), fragment!!)
            .addToBackStack(screen.screenKey)
            .commit()
        localStackCopy.add(screen.screenKey)
    }

    protected fun fragmentBack() {
        if (localStackCopy.size > 0) {
            fragmentManager.popBackStack()
            localStackCopy.removeLast()
        } else {
            activityBack()
        }
    }

    protected fun activityBack() {
        activity.finish()
    }

    protected fun activityReplace(command: Replace) {
        val screen = command.screen
        val activityIntent = screen.getActivityIntent(activity)

        // Replace activity
        if (activityIntent != null) {
            val options = createStartActivityOptions(command, activityIntent)
            checkAndStartActivity(screen, activityIntent, options)
            activity.finish()
        } else {
            fragmentReplace(command)
        }
    }

    protected fun fragmentReplace(command: Replace) {
        val screen = command.screen
        val fragment = createFragment(screen)

        if (localStackCopy.size > 0) {
            fragmentManager.popBackStack()
            localStackCopy.removeLast()

            val fragmentTransaction = fragmentManager.beginTransaction()

            setupFragmentTransaction(
                command,
                fragmentManager.findFragmentById(getContainerId(command.target)),
                fragment,
                fragmentTransaction
            )

            fragmentTransaction
                .replace(getContainerId(command.target), fragment!!)
                .addToBackStack(screen.screenKey)
                .commit()
            localStackCopy.add(screen.screenKey)

        } else {
            val fragmentTransaction = fragmentManager.beginTransaction()

            setupFragmentTransaction(
                command,
                fragmentManager.findFragmentById(getContainerId(command.target)),
                fragment,
                fragmentTransaction
            )

            fragmentTransaction
                .replace(getContainerId(command.target), fragment!!)
                .commit()
        }
    }

    /**
     * Performs [BackTo] command transition
     */
    protected fun backTo(command: BackTo) {
        val key = command.screen.screenKey
        val index = localStackCopy.indexOf(key)
        val size = localStackCopy.size

        if (index != -1) {
            for (i in 1 until size - index) {
                localStackCopy.removeLast()
            }
            fragmentManager.popBackStack(key, 0)
        } else {
            backToUnexisting(command.screen)
        }
    }

    private fun backToRoot() {
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        localStackCopy.clear()
    }

    /**
     * Override this method to setup fragment transaction [FragmentTransaction].
     * For example: setCustomAnimations(...), addSharedElement(...) or setReorderingAllowed(...)
     *
     * @param command             current navigation command. Will be only [Forward] or [Replace]
     * @param currentFragment     current fragment in container
     * (for [Replace] command it will be screen previous in new chain, NOT replaced screen)
     * @param nextFragment        next screen fragment
     * @param fragmentTransaction fragment transaction
     */
    @Suppress("UNUSED_PARAMETER", "ProtectedInFinal")
    protected fun setupFragmentTransaction(command: TargetCommand, currentFragment: Fragment?, nextFragment: Fragment?, fragmentTransaction: FragmentTransaction) {
    }

    /**
     * Override this method to create option for start activity
     *
     * @param command        current navigation command. Will be only [Forward] or [Replace]
     * @param activityIntent activity intent
     * @return transition options
     */
    @Suppress("UNUSED_PARAMETER", "ProtectedInFinal")
    protected fun createStartActivityOptions(command: TargetCommand, activityIntent: Intent): Bundle? {
        return null
    }

    private fun checkAndStartActivity(screen: SupportAppScreen, activityIntent: Intent, options: Bundle?) {
        // Check if we can start activity
        if (activityIntent.resolveActivity(activity.packageManager) != null) {
            activity.startActivity(activityIntent, options)
        } else {
            unexistingActivity(screen, activityIntent)
        }
    }

    /**
     * Called when there is no activity to open `screenKey`.
     *
     * @param screen         screen
     * @param activityIntent intent passed to start Activity for the `screenKey`
     */
    @Suppress("UNUSED_PARAMETER", "ProtectedInFinal")
    protected fun unexistingActivity(screen: SupportAppScreen, activityIntent: Intent) {
        // Do nothing by default
    }

    /**
     * Creates Fragment matching `screenKey`.
     *
     * @param screen screen
     * @return instantiated fragment for the passed screen
     */
    @Suppress("UNUSED_PARAMETER", "ProtectedInFinal")
    protected fun createFragment(screen: SupportAppScreen): Fragment? {
        val fragment = screen.fragment

        if (fragment == null) {
            errorWhileCreatingScreen(screen)
        }
        return fragment
    }

    /**
     * Called when we tried to fragmentBack to some specific screen (via [BackTo] command),
     * but didn't found it.
     *
     * @param screen screen
     */
    @Suppress("UNUSED_PARAMETER", "ProtectedInFinal")
    protected fun backToUnexisting(screen: SupportAppScreen) {
        backToRoot()
    }

    @Suppress("UNUSED_PARAMETER", "ProtectedInFinal")
    protected fun errorWhileCreatingScreen(screen: SupportAppScreen) {
        throw RuntimeException("Can't create a screen: " + screen.screenKey)
    }
}
