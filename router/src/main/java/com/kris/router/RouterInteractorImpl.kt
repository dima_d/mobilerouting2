package com.kris.router

import android.util.Log
import com.kris.api.RouterInteractor
import com.kris.api.RouterModule
import com.kris.api.utils.plusAssign
import com.kris.declaration.RouterEvent
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import ru.terrakok.cicerone.Router
import java.util.concurrent.ConcurrentHashMap
import kotlin.reflect.KClass

class RouterInteractorImpl(
    override val router: Router,
    eventsScheduler: Scheduler = AndroidSchedulers.mainThread()
) :
    RouterInteractor {
    private val disposer = CompositeDisposable()
    private val events = PublishSubject.create<RouterEvent>()
    private var tag: String? = null

    private val modules = ConcurrentHashMap<KClass<out RouterModule>, RouterModule>()

    init {
        disposer += events.observeOn(eventsScheduler).subscribe({ modules.values.firstOrNull { state -> state.contains(it) }?.fire(it) }, { it.printStackTrace() })
    }

    override fun putModule(routerModule: RouterModule) = modules.put(routerModule::class, routerModule)

    override fun enableLogging(tag: String) {
        this.tag = tag
    }

    override fun stop() {
        tag?.let { Log.d(it, "%s stop() called"); }
        disposer.dispose()
    }

    override fun getModule(module: KClass<out RouterModule>): RouterModule? = modules[module]

    override fun fire(event: RouterEvent) {
        tag?.let { Log.d(it, "%s  Event ${event.javaClass.simpleName} called for state ${modules.values.first { state -> state.contains(event) }?.javaClass?.simpleName} ${System.identityHashCode(this)} ") }
        events.onNext(event)
    }
}